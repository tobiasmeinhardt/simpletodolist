import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { reducer } from './modules/TodoHandling'
import'./assts/styles/index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

export const store = createStore(reducer);

ReactDOM.render(    
    <Provider store= { store }>
        <App />
    </Provider>,       
     document.getElementById('root')
);
registerServiceWorker();



