import React from 'react';
import '../assts/styles/App.css';
import TodoListContainer from '../containers/TodoListContainer';

class App extends React.Component {
  render = () =>
  {
    //var todos = [new Todo("Schlagbohrer kaufen", false)];
    return (
      <div className="App">
        <header className="App-header">          
          <h1 className="App-title">Simple Todo-List</h1>
        </header>
        <TodoListContainer/>        
      </div>
    );
  }
}

export default App;
