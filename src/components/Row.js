import React, { Component } from 'react';
import '../assts/styles/Row.css';

class Row extends Component 
{    
    renderTextContent = () =>
    {    
        if(!this.props.inEditMode)
        {
            return (<p className="text-content">{ this.props.title }</p>);
        }
        return (
            <form>                                   
                <input 
                    className="text-input" 
                    type="text" 
                    value={ this.props.title } 
                    name="title" 
                    onChange={ this.props.onTitleChange }
                />
            </form>
        );
    }

    render = () =>
    {                           
        return(
            <li className={this.props.className} key={this.props.key}  >
                <div className="left-icon">
                    { this.props.leftItem }                
                </div> 
                <div className={ "title-section" } >
                    { this.renderTextContent() }                    
                </div> 
                <div className="center-item" >                    
                    { this.props.centerItem }
                </div>                 
                <div className="right-item">                    
                    { this.props.rightItem }
                    
                </div>
            </li>
        );
    }
}

export default Row;