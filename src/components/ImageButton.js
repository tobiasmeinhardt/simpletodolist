import React, { Component } from 'react';
import '../assts/styles/ImageButton.css';

class ImageButton extends Component 
{

    render = () =>
    {
        return (
            <div className="imageButton" onClick= { this.props.onClick } >
                <img src={ this.props.imageSource } className="icon" alt="" />
            </div>
        );
    }
}

export default ImageButton;