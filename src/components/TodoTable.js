import React from 'react';
import editIcon from '../assts/icons/edit.png';
import removeButtonIcon from '../assts/icons/removeButton.png';
import Row from '../components/Row';
import NavBox from '../components/NavBox';
import TextButton from '../components/TextButton';
import ImageButton from '../components/ImageButton';
import '../assts/styles/TodoTable.css';

class TodoTable extends React.Component
{
    rowForIndex = (index) =>
    {        
        //returns a row for the index
        let color = index % 2 === 0 ? 'even' : 'odd';
        let todo = this.props.todos[index];
        return <Row title={ todo.title } 
            key = { index }
            className={ color }
            inEditMode={this.props.todos[index].inEditMode}
            onTitleChange={ (event) => this.props.onTodoTitleChange(index, event.target.value)        
            }           
            rightItem={
                <ImageButton onClick= {
                    (index) => this.props.onRemoveRowClick(index)} 
                    imageSource={ removeButtonIcon }
                />} 
            centerItem={ 
                <ImageButton onClick= {() => this.props.onToggleEditModeClick(index)} 
                    imageSource= { editIcon }
                /> }
            leftItem= {
                <NavBox onDownClick= 
                    { () => this.props.onMoveRowDownClick(index) } 
                    onUpClick=
                    { () => this.props.onMoveRowUpClick(index) } 
                />}            
        />;
    }

    renderToDos = () =>
    {                
        var allRows = [];                        
        for(var i= 0; i<this.props.todos.length; i++)
        {               
            allRows.push(this.rowForIndex(i));            
        }
        return allRows;
    }

    render = () =>
    {          
        return (            
            <ul className='TodoTable'>                                
                { this.renderToDos() }
                <li className="footer-section">
                    <TextButton text="Eintrag hinzufügen" onClick={ () => this.props.onAddTodoClick('', true) }/>
                </li>‚
            </ul>
        );
    }    
}

export default TodoTable;